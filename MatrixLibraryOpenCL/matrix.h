//
//  matrix.h
//  ANN
//
//  Created by Nicholas Wilkerson and Dave Heyborne on 12/21/14.
//  Copyright (c) 2014 Nicholas Wilkerson and Dave Heyborne. All rights reserved.
//

#ifndef __ANN__matrixOperations__
#define __ANN__matrixOperations__

#include <OpenCL/opencl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kernels.cl.h"
#include "matrixtypes.h"



#pragma mark OpenCL Initialization

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *  Creates a dispatch queue using the last GPU discovered
 *  Make sure the queue is released using 
 *  dispatch_release(dispatch_queue_t)
 *  or bad things will happen
 */
dispatch_queue_t openCLInit();

#pragma mark Vector Creation

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
MatrixFloat *createVectorFloat(INT size, enum VectorOrientation orientation);

/*
 *
 */
MatrixInt *createVectorInt(INT size, enum VectorOrientation orientation);

/*
 *
 */
MatrixFloat *createVectorZerosFloat(INT size, VectorOrientation orientation);

/*
 *
 */
MatrixInt *createVectorZerosInt(INT size, VectorOrientation orientation);

/*
 *
 */
MatrixFloat *createVectorOnesFloat(INT size, VectorOrientation orientation);

/*
 *
 */
MatrixInt *createVectorOnesInt(INT size, VectorOrientation orientation);

/*
 *
 */
int setAllVectorFloatValuesTo(MatrixFloat *vector, FLOAT value);

/*
 *
 */
int setAllVectorIntValuesTo(MatrixInt *vector, INT value);

#pragma mark Matrix Creation

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
MatrixFloat *createMatrixFloat(INT width, INT height);

/*
 *
 */
MatrixInt *createMatrixInt(INT width, INT height);

/*
 *
 */
MatrixFloat *createMatrixZerosFloat(INT width, INT height);

/*
 *
 */
MatrixInt *createMatrixZerosInt(INT width, INT height);

/*
 *
 */
MatrixFloat *createMatrixOnesFloat(INT width, INT height);

/*
 *
 */
MatrixInt *createMatrixOnesInt(INT width, INT height);

/*
 *
 */
void setAllMatrixFloatValuesTo(MatrixFloat *matrix, FLOAT value);

/*
 *
 */
void setAllMatrixIntValuesTo(MatrixInt *matrix, INT value);

#pragma mark Matrix Deletion

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
int deleteMatrixFloat(MatrixFloat *matrix);

/*
 *
 */
int deleteMatrixInt(MatrixInt *matrix);

#pragma mark Matrix Printing

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
void printMatrixFloat(MatrixFloat *matrix);

/*
 *
 */
void printMatrixInt(MatrixInt *matrix);

/*
 *
 */
void printUpperLeftSquareFloat(MatrixFloat *matrix, INT size);

/*
 *
 */
void printUpperLeftSquareInt(MatrixInt *matrix, INT size);

#pragma mark Vector-Only Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
int transpose1DFloat(MatrixFloat *vector);

/*
 *
 */
int transpose1DInt(MatrixInt *vector);

/*
 *  input1 and input2 are arrays of size arraySize
 *  result is the address of a float
 */
int dotFloat(MatrixFloat *input1,
             MatrixFloat *input2,
             FLOAT *result,
             dispatch_queue_t queue);

/*
 *  Multiplies every element of an array by a scalar value
 */
int scalarMultiply1DFloat(MatrixFloat *inputMatrix,
                          FLOAT scalar,
                          MatrixFloat *matrixResult,
                          dispatch_queue_t queue);

/*
 *  Multiplies every element of an array by a scalar value
 */
int scalarMultiply1DInt(MatrixInt *inputMatrix,
                        int scalar,
                        MatrixInt *matrixResult,
                        dispatch_queue_t queue);

/*
 *  input1 and input2 and output are arrays of size arraySize
 *
 */
int elementMultiply1DFloat(MatrixFloat *input1,
                           MatrixFloat *input2,
                           MatrixFloat *result,
                           dispatch_queue_t queue);

/*
 *  input1 and input2 and output are arrays of size arraySize
 *
 */
int elementMultiply1DInt(MatrixInt *input1,
                         MatrixInt *input2,
                         MatrixInt *result,
                         dispatch_queue_t queue);

#pragma mark Matrix Functions - One Matrix

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *  Transposes
 */
int transpose2DFloat(MatrixFloat *matrixInput,
                     MatrixFloat *matrixResult,
                     dispatch_queue_t queue);

/*
 *  Transposes
 */
int transpose2DInt(MatrixInt *matrixInput,
                   MatrixInt *matrixResult,
                   dispatch_queue_t queue);

/*
 *  Sum of every element in the matrix
 */
float elementSum2DFloat(MatrixFloat *matrix,
                        dispatch_queue_t queue);

/*
 *  Sum of every element in the matrix
 */
int elementSum2DInt(MatrixInt *matrix,
                    dispatch_queue_t queue);

#pragma mark Matrix Functions - Scalars

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *  Adds a scalar to every element of a matrix
 */
int scalarAdd2DFloat(MatrixFloat *inputMatrix,
                     FLOAT scaler,
                     MatrixFloat *matrixResult,
                     dispatch_queue_t queue);

/*
 *  Adds a scalar to every element of a matrix
 */
int scalarAdd2DInt(MatrixInt *inputMatrix,
                   INT scaler,
                   MatrixInt *matrixResult,
                   dispatch_queue_t queue);

/*
 *  Multiplies every element of a matrix by a scalar value
 */
int scalarMultiply2DFloat(MatrixFloat *inputMatrix,
                          FLOAT scalar,
                          MatrixFloat *matrixResult,
                          dispatch_queue_t queue);

/*
 *  Multiplies every element of a matrix by a scalar value
 */
int scalarMultiply2DInt(MatrixInt *inputMatrix,
                        INT scalar,
                        MatrixInt *matrixResult,
                        dispatch_queue_t queue);

#pragma mark Matrix Functions - Per-Element

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *  adds two matrices of equal size
 */
int add2DFloat(MatrixFloat *input1,
               MatrixFloat *input2,
               MatrixFloat *output,
               dispatch_queue_t queue);

/*
 *  adds two matrices of equal size
 */
int add2DInt(MatrixInt *input1,
             MatrixInt *input2,
             MatrixInt *output,
             dispatch_queue_t queue);

/*
 *  subtracts two matrices of equal size
 */
int subtract2DFloat(MatrixFloat *input1,
                    MatrixFloat *input2,
                    MatrixFloat *output,
                    dispatch_queue_t queue);

/*
 *  subtracts two matrices of equal size
 */
int subtract2DInt(MatrixInt *input1,
                  MatrixInt *input2,
                  MatrixInt *output,
                  dispatch_queue_t queue);

/*
 *  multiplies two matrices element by element
 *  not a cross product
 */
int elementMultiply2DFloat(MatrixFloat *input1,
                           MatrixFloat *input2,
                           MatrixFloat *output,
                           dispatch_queue_t queue);

/*
 *  multiplies two matrices element by element
 *  not a cross product
 */
int elementMultiply2DInt(MatrixInt *input1,
                         MatrixInt *input2,
                         MatrixInt *output,
                         dispatch_queue_t queue);

#pragma mark Matrix Functions - Vectors

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *  Sums rows of a two dimensional matrix to form a colum vector
 */
int sumRows2DFloat(MatrixFloat* matrixInput,
                   MatrixFloat *vectorResult,
                   dispatch_queue_t queue);

/*
 *  Sums rows of a two dimensional matrix to form a column vector
 */
int sumRows2DInt(MatrixInt* matrixInput,
                 MatrixInt *vectorResult,
                 dispatch_queue_t queue);

/*
 *  Sums columns of a two dimensional matrix to form a row vector
 */
int sumColumns2DFloat(MatrixFloat* matrixInput,
                      MatrixFloat *vectorResult,
                      dispatch_queue_t queue);

/*
 *  Sums columns of a two dimensional matrix to form a row vector
 */
int sumColumns2DInt(MatrixInt* matrixInput,
                    MatrixInt *vectorResult,
                    dispatch_queue_t queue);

/*
 *  This function adds a column vector to a matrix column my column
 */
int vectorColumnAddToMatrixFloat(MatrixFloat *matrixInput,
                                 MatrixFloat *vector,
                                 MatrixFloat *matrixResult,
                                 dispatch_queue_t queue);

/*
 *  This function adds a row vector to a matrix row by row
 */
int vectorRowAddToMatrixFloat(MatrixFloat *matrixInput,
                              MatrixFloat *vector,
                              MatrixFloat *matrixResult,
                              dispatch_queue_t queue);

#pragma mark Matrix Functions - Two Matrices

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

// TODO: Benchmark the GPU functions and CPU functions

/*
 *  Performs Matrix Multiplication
 *  The width of the first matrix must match the height of the second matrix
 */
int multiply2DFloat(MatrixFloat *input1,
                    MatrixFloat *input2,
                    MatrixFloat *output,
                    dispatch_queue_t queue);

/*
 *  Performs Matrix Multiplication
 *  The width of the first matrix must match the height of the second matrix
 */
int multiply2DFloat_GPU(MatrixFloat *input1,
                    MatrixFloat *input2,
                    MatrixFloat *output,
                    dispatch_queue_t queue);

/*
 *  Performs Matrix Multiplication using the CPU not OpenCL
 *  The width of the first matrix must match the height of the second matrix
 */
int multiply2DFloat_CPU(MatrixFloat *input1,
                        MatrixFloat *input2,
                        MatrixFloat *result);

/*
 *  Performs Matrix Multiplication
 *  The width of the first matrix must match the height of the second matrix
 */
int multiply2DInt(MatrixInt *input1,
                      MatrixInt *input2,
                      MatrixInt *output,
                      dispatch_queue_t queue);

/*
 *  Performs Matrix Multiplication
 *  The width of the first matrix must match the height of the second matrix
 */
int multiply2DInt_GPU(MatrixInt *input1,
                  MatrixInt *input2,
                  MatrixInt *output,
                  dispatch_queue_t queue);

#pragma mark Matrix Append Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
int appendHorizontalFloat(MatrixFloat *leftMatrix, MatrixFloat *rightMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendHorizontalInt(MatrixInt *leftMatrix, MatrixInt *rightMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendVerticalFloat(MatrixFloat *upperMatrix, MatrixFloat *lowerMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendVerticalInt(MatrixInt *upperMatrix, MatrixInt *lowerMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendHorizontalFloat_CPU(MatrixFloat *leftMatrix, MatrixFloat *rightMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendHorizontalInt_CPU(MatrixInt *leftMatrix, MatrixInt *rightMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendVerticalFloat_CPU(MatrixFloat *upperMatrix, MatrixFloat *lowerMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue);

/*
 *
 */
int appendVerticalInt_CPU(MatrixInt *upperMatrix, MatrixInt *lowerMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue);

#pragma mark Determinant Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
int determinant2DFloat(MatrixFloat *matrix, FLOAT *result, dispatch_queue_t queue);

/*
 *
 */
int determinant2DInt(MatrixInt *matrix, INT *result, dispatch_queue_t queue);

/*
 *
 */
int determinantChio2DFloat(MatrixFloat *matrix, FLOAT *result, dispatch_queue_t queue);

/*
 *
 */
int determinantChio2DInt(MatrixInt *matrix, INT *result, dispatch_queue_t queue);

#pragma mark Helper Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

/*
 *
 */
cl_ndrange createRange1D(INT size, void *kernelFunction);

/*
 *
 */
cl_ndrange createRangeSquare2D(INT size, void *kernelFunction);

/*
 *
 */
cl_ndrange createRange2D(INT width, INT height, void *kernelFunction);

#endif /* defined(__ANN__matrixOperations__) */
