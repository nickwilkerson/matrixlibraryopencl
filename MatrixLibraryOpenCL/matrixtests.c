//
//  matrixtests.c
//  ANN
//
//  Created by Nicholas Wilkerson and Dave Heyborne on 12/21/14.
//  Copyright (c) 2014 Nicholas Wilkerson and Dave Heyborne. All rights reserved.
//

#include "matrixtests.h"

static dispatch_queue_t queue = NULL;
static const char *currentTestName;

void setUpTests() {
    if (queue == NULL) {queue = openCLInit();}
    else {printf("Error: setUp called, but queue already exists.\n");}
}

void tearDownTests() {
    dispatch_release(queue);
    queue = NULL;
}

void beginTest(const char *const testName) {
    currentTestName = testName;
    printf("\nTesting %s:\n", currentTestName);
}

void runMatrixTests() {
    setUpTests();
    
    /* Vector Creation Tests */
    beginTest("createVectorFloat");
    testCreateVectorFloat();
    
    beginTest("createVectorInt");
    testCreateVectorInt();
    
    beginTest("createVectorZerosFloat");
    testCreateVectorZerosFloat();
    
    beginTest("createVectorZerosInt");
    testCreateVectorZerosInt();
    
    beginTest("createVectorOnesFloat");
    testCreateVectorOnesFloat();
    
    beginTest("createVectorOnesInt");
    testCreateVectorOnesInt();
    
    beginTest("setAllVectorFloatValuesTo");
    testSetAllVectorFloatValuesTo();
    
    beginTest("setAllVectorIntValuesTo");
    testSetAllVectorIntValuesTo();
    
    /* Matrix Creation Tests */
    beginTest("createMatrixFloat");
    testCreateMatrixFloat();
    
    beginTest("createMatrixInt");
    testCreateMatrixInt();
    
    beginTest("createMatrixZerosFloat");
    testCreateMatrixZerosFloat();
    
    beginTest("createMatrixZerosInt");
    testCreateMatrixZerosInt();
    
    beginTest("createMatrixOnesFloat");
    testCreateMatrixOnesFloat();
    
    beginTest("createMatrixOnesInt");
    testCreateMatrixOnesInt();
    
    beginTest("createMatrixFloatFromVector");
    testCreateMatrixFloatFromVector();
    
    beginTest("createMatrixIntFromVector");
    testCreateMatrixIntFromVector();
    
    beginTest("setAllMatrixFloatValuesTo");
    testSetAllMatrixFloatValuesTo();
    
    beginTest("setAllMatrixIntValuesTo");
    testSetAllMatrixIntValuesTo();
    
    /* Vector Deletion Tests */
    beginTest("deleteVectorFloat");
    testDeleteVectorFloat();
    
    beginTest("deleteVectorInt");
    testDeleteVectorInt();
    
    /* Matrix Deletion Tests */
    beginTest("deleteMatrixFloat");
    testDeleteMatrixFloat();
    
    beginTest("deleteMatrixInt");
    testDeleteMatrixInt();
    
    /* Vector Printing Tests */
    beginTest("printVectorFloat");
    testPrintVectorFloat();
    
    beginTest("printVectorInt");
    testPrintVectorInt();
    
    /* Matrix Printing Tests */
    beginTest("printMatrixFloat");
    testPrintMatrixFloat();
    
    beginTest("printMatrixInt");
    testPrintMatrixInt();
    
    beginTest("printUpperLeftSquareFloat");
    testPrintUpperLeftSquareFloat();
    
    beginTest("printUpperLeftSquareInt");
    testPrintUpperLeftSquareInt();
    
    /* Vector-Only Function Tests */
    beginTest("transpose1DFloat");
    testTranspose1DFloat();
    
    beginTest("transpose1DInt");
    testTranspose1DInt();
    
    beginTest("dotFloat");
    testDotFloat();
    
    beginTest("scalarMultiply1DFloat");
    testScalarMultiply1DFloat();
    
    beginTest("scalarMultiply1DInt");
    testScalarMultiply1DInt();
    
    beginTest("elementMultiply1DFloat");
    testElementMultiply1DFloat();
    
    beginTest("elementMultiply1DInt");
    testElementMultiply1DInt();
    
    /* Matrix Functions - One Matrix Tests */
    beginTest("transpose2DFloat");
    testTranspose2DFloat();
    
    beginTest("transpose2DInt");
    testTranspose2DInt();
    
    beginTest("elementSum2DFloat");
    testElementSum2DFloat();
    
    beginTest("elementSum2DInt");
    testElementSum2DInt();
    
    /* Matrix Functions - Scalar Tests */
    beginTest("scalarAdd2DFloat");
    testScalarAdd2DFloat();
    
    beginTest("scalarAdd2DInt");
    testScalarAdd2DInt();
    
    beginTest("scalarMultiply2DFloat");
    testScalarMultiply2DFloat();
    
    beginTest("scalarMultiply2DInt");
    testScalarMultiply2DInt();
    
    /* Matrix Functions - Per-Element Tests */
    beginTest("add2DFloat");
    testAdd2DFloat();
    
    beginTest("add2DInt");
    testAdd2DInt();
    
    beginTest("subtract2DFloat");
    testSubtract2DFloat();
    
    beginTest("subtract2DInt");
    testSubtract2DInt();
    
    beginTest("elementMultiply2DFloat");
    testElementMultiply2DFloat();
    
    beginTest("elementMultiply2DInt");
    testElementMultiply2DInt();
    
    /* Matrix Functions - Vector Tests */
    beginTest("sumRows2DFloat");
    testSumRows2DFloat();
    
    beginTest("sumRows2DInt");
    testSumRows2DInt();
    
    beginTest("sumColumns2DFloat");
    testSumColumns2DFloat();
    
    beginTest("sumColumns2DInt");
    testSumColumns2DInt();
    
    beginTest("vectorColumnAddToMatrixFloat");
    testVectorColumnAddToMatrixFloat();
    
    beginTest("vectorRowAddToMatrixFloat");
    testVectorRowAddToMatrixFloat();
    
    /* Matrix Functions - Two Matrices Tests */
    beginTest("multiply2DFloat");
    testMultiply2DFloat();
    
    beginTest("multiply2DFloat_CPU");
    testMultiply2DFloat_CPU();
    
    beginTest("multiply2DInt");
    testMultiply2DInt();
    
    /* Matrix Append Function Tests */
    beginTest("appendHorizontalFloat");
    testAppendHorizontalFloat();
    
    beginTest("appendHorizontalInt");
    testAppendHorizontalInt();
    
    beginTest("appendVerticalFloat");
    testAppendVerticalFloat();
    
    beginTest("appendVerticalInt");
    testAppendVerticalInt();
    
    /* Determinant Function Tests */
    beginTest("determinant2DFloat");
    testDeterminant2DFloat();
    
    beginTest("determinant2DInt");
    testDeterminant2DInt();
    
    beginTest("determinantChio2DFloat");
    testDeterminantChio2DFloat();
    
    beginTest("determinantChio2DInt");
    testDeterminantChio2DInt();
    
    /* Helper Function Tests */
    beginTest("createRange1D");
    testCreateRange1D();
    
    beginTest("createRangeSquare2D");
    testCreateRangeSquare2D();
    
    beginTest("createRange2D");
    testCreateRange2D();
    
    tearDownTests();
}

#pragma mark Vector Creation Tests

void testCreateVectorFloat() {
    
}

void testCreateVectorInt() {

}

void testCreateVectorZerosFloat() {

}

void testCreateVectorZerosInt() {

}

void testCreateVectorOnesFloat() {

}

void testCreateVectorOnesInt() {

}

void testSetAllVectorFloatValuesTo() {

}

void testSetAllVectorIntValuesTo() {

}

#pragma mark Matrix Creation Tests

void testCreateMatrixFloat() {

}

void testCreateMatrixInt() {

}

void testCreateMatrixZerosFloat() {

}

void testCreateMatrixZerosInt() {

}

void testCreateMatrixOnesFloat() {

}

void testCreateMatrixOnesInt() {

}

void testCreateMatrixFloatFromVector() {

}

void testCreateMatrixIntFromVector() {

}

void testSetAllMatrixFloatValuesTo() {

}

void testSetAllMatrixIntValuesTo() {

}

#pragma mark Vector Deletion Tests

void testDeleteVectorFloat() {

}

void testDeleteVectorInt() {

}

#pragma mark Matrix Deletion Tests

void testDeleteMatrixFloat() {

}

void testDeleteMatrixInt() {

}

#pragma mark Vector Printing Tests

void testPrintVectorFloat() {

}

void testPrintVectorInt() {

}

#pragma mark Matrix Printing Tests

void testPrintMatrixFloat() {

}

void testPrintMatrixInt() {

}

void testPrintUpperLeftSquareFloat() {

}

void testPrintUpperLeftSquareInt() {

}

#pragma mark Vector-Only Function Tests

void testTranspose1DFloat() {
    
}

void testTranspose1DInt() {
    
}

void testDotFloat() {
    int size = 1;
    MatrixFloat *vector1 = createVectorFloat(size, ROW);
    MatrixFloat *vector2 = createVectorFloat(size, ROW);
    float correctResult = 0;
    float result = -1.0;
    
    for (int i = 0; i < size; ++i) {
        vector1->contents[i] = i;
        vector2->contents[i] = i;
        correctResult += (i * i);
    }
    dotFloat(vector1, vector2, &result, queue);
    
    if (result == correctResult) {
        printf("dotFloat Test Passed.\n");
    } else {
        printf("dotFloat Test Failed.\n");
    }
    deleteMatrixFloat(vector1);
    deleteMatrixFloat(vector2);
}

void testScalarMultiply1DFloat() {
    
}

void testScalarMultiply1DInt() {
    
}

void testElementMultiply1DFloat() {
    
}

void testElementMultiply1DInt() {
    
}

#pragma mark Matrix Functions - One Matrix Tests

void testTranspose2DFloat() {
    
}

void testTranspose2DInt() {
    
}

void testElementSum2DFloat() {
    
}

void testElementSum2DInt() {
    
}

#pragma mark Matrix Functions - Scalar Tests

void testScalarAdd2DFloat() {
    
}

void testScalarAdd2DInt() {
    
}

void testScalarMultiply2DFloat() {
    
}

void testScalarMultiply2DInt() {
    
}

#pragma mark Matrix Functions - Per-Element Tests

void testAdd2DFloat() {
    MatrixFloat *matrix1 = createMatrixFloat(3, 3);
    for (int i = 0; i < matrix1->width * matrix1->height; i++) {
        matrix1->contents[i] = i;
    }
    
    MatrixFloat *output = createMatrixFloat(matrix1->width, matrix1->height);
    
    add2DFloat(matrix1, matrix1, output, queue);
    
    MatrixFloat *solution = createMatrixFloat(output->width, output->height);
    for (int i = 0; i < solution->width * solution->height; i++) {
        solution->contents[i] = i * 2;
    }
    
    int pass = true;
    for (int i = 0; i < solution->width * solution->height; i++) {
        if (output->contents[i] != solution->contents[i]) {
            pass = false;
            break;
        }
    }
    
    if (pass == true) {
        printf("add2DFloat Test Passed.\n");
    } else {
        printf("add2DFloat Test Failed.\n");
    }
    deleteMatrixFloat(matrix1);
    deleteMatrixFloat(output);
    deleteMatrixFloat(solution);
}

void testAdd2DInt() {
    
}

void testSubtract2DFloat() {
    
}

void testSubtract2DInt() {
    
}

void testElementMultiply2DFloat() {
    
}

void testElementMultiply2DInt() {
    
}

#pragma mark Matrix Functions - Vector Tests

void testSumRows2DFloat() {
    MatrixFloat *matrix = createMatrixFloat(200, 100);
    MatrixFloat *vectorResult = createVectorFloat(100, COLUMN);
    MatrixFloat *vectorSolution = createVectorFloat(100, COLUMN);
    for (int i = 0; i < matrix->width * matrix->height; i++) {
        matrix->contents[i] = i;
    }
    
    sumRows2DFloat(matrix, vectorResult, queue);
    
    for (int row = 0; row < matrix->height; row++) {
        float sum = 0;
        for (int column = 0; column < matrix->width; column++) {
            sum += matrix->contents[row * matrix->width + column];
        }
        vectorSolution->contents[row] = sum;
    }
    
    char pass = true;
    for (int i = 0; i < vectorResult->width * vectorResult->height; i++) {
        if (vectorResult->contents[i] != vectorSolution->contents[i]) {
            pass = false;
            break;
        }
    }
    if (pass == true) {
        printf("sumRows2DFloat Test Passed.\n");
    } else {
        printf("sumRows2DFloat Test Failed.\n");
    }
    deleteMatrixFloat(matrix);
    deleteMatrixFloat(vectorResult);
    deleteMatrixFloat(vectorSolution);
}

void testSumRows2DInt() {
    
}

void testSumColumns2DFloat() {
    
}

void testSumColumns2DInt() {
    
}

void testVectorColumnAddToMatrixFloat(){
    
}

void testVectorRowAddToMatrixFloat() {
    
}

#pragma mark Matrix Functions - Two Matrices Tests

void testMultiply2DFloat() {
    MatrixFloat *matrix1 = createMatrixFloat(400, 700);
    for (int i = 0; i < matrix1->width * matrix1->height; i++) {
        matrix1->contents[i] = i;
    }
    
    MatrixFloat *matrix2 = createMatrixFloat(600, 400);
    
    for (int i = 0; i < matrix2->width * matrix2->height; i++) {
        matrix2->contents[i] = i;
    }
    
    MatrixFloat *output = createMatrixFloat(matrix2->width, matrix1->height);
    
    if (multiply2DFloat(matrix1, matrix2, output, queue) < 0) {
        printf("error\n");
    }

    MatrixFloat *solution = createMatrixFloat(output->width, output->height);
    multiply2DFloat_CPU(matrix1, matrix2, solution);
 
    int pass = true;
    for (int i = 0; i < solution->width * solution->height; i++) {
        if (output->contents[i] != solution->contents[i]) {
            pass = false;
            break;
        }
    }

    if (pass == true) {
        printf("multiply2DFloat Test Passed.\n");
    } else {
        printf("multiply2DFloat Test Failed:\n");
        printf("Matrix 1:\n");
        printMatrixFloat(matrix1);
        printf("\nMatrix 2:\n");
        printMatrixFloat(matrix2);
        printf("\nResult Matrix:\n");
        printMatrixFloat(output);
        printf("\nSolution Matrix\n");
        printMatrixFloat(solution);
        printf("\n");
    }
    deleteMatrixFloat(matrix1);
    deleteMatrixFloat(matrix2);
    deleteMatrixFloat(output);
    deleteMatrixFloat(solution);
}

void testMultiply2DFloat_CPU() {
    
}

void testMultiply2DInt() {
    
}

#pragma mark Matrix Append Function Tests

void testAppendHorizontalFloat() {
    
}

void testAppendHorizontalInt() {
    
}

void testAppendVerticalFloat() {
    
}

void testAppendVerticalInt() {
    
}

#pragma mark Determinant Function Tests

void testDeterminant2DFloat() {
    int failCount = 0;
    
    MatrixFloat *matrix0 = createMatrixZerosFloat(3, 3);
    MatrixFloat *matrix1 = createMatrixOnesFloat(3, 3);
    MatrixFloat *matrix2 = createMatrixFloat(4, 4);
    MatrixFloat *matrix3 = createMatrixFloat(6, 6);
    MatrixFloat *matrix4 = createMatrixFloat(6, 6);

    float matrix0DetActual = 0;
    float matrix1DetActual = 0;
    
    memcpy(matrix2->contents, (float [16]){ 1,9,7,1,
                                            4,1,5,1,
                                            1,6,1,1,
                                            2,1,7,3}, 16*sizeof(float));
    float matrix2DetActual = 384;
    
    memcpy(matrix3->contents, (float [36]){ 5,2,9,1,5,8,
                                            4,2,8,8,7,5,
                                            7,4,5,6,8,6,
                                            3,9,2,7,4,0,
                                            0,1,3,2,1,3,
                                            8,4,5,5,6,9}, 36*sizeof(float));
    float matrix3DetActual = 10769;
    
    memcpy(matrix4->contents, (float [36]){ 5,2,9,-1,5,8,
                                            4,2,8,8,7,5,
                                            7,4,5,6,8,6,
                                            3,9,2,7,4,0,
                                            0,1,3,2,1,3,
                                            8,4,-5,5,6,9}, 36*sizeof(float));
    float matrix4DetActual = 3343;
    
    printf("\ntesting matrix 0\n");
    float matrix0Det;
    determinant2DFloat(matrix0, &matrix0Det, queue);
    printf("matrix0Det = %f\n", matrix0Det);
    if (roundf(matrix0Det) != matrix0DetActual) {
        printf("matrix 0 failed\nActual matrix 0 Determinant %f\n\n", matrix0DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 1\n");
    float matrix1Det;
    determinant2DFloat(matrix1, &matrix1Det, queue);
    printf("matrix1Det = %f\n", matrix1Det);
    if (roundf(matrix1Det) != matrix0DetActual) {
        printf("matrix 1 failed\nActual matrix 0 Determinant %f\n\n", matrix1DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 2\n");
    float matrix2Det;
    determinant2DFloat(matrix2, &matrix2Det, queue);
    printf("matrix2Det = %f\n", matrix2Det);
    if (roundf(matrix2Det) != matrix0DetActual) {
        printf("matrix 2 failed\nActual matrix 0 Determinant %f\n\n", matrix2DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 3\n");
    float matrix3Det;
    determinant2DFloat(matrix3, &matrix3Det, queue);
    printf("matrix3Det = %f\n", matrix3Det);
    if (roundf(matrix3Det) != matrix0DetActual) {
        printf("matrix 3 failed\nActual matrix 0 Determinant %f\n\n", matrix3DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 4\n");
    float matrix4Det;
    determinant2DFloat(matrix4, &matrix4Det, queue);
    printf("matrix4Det = %f\n", matrix4Det);
    if (roundf(matrix4Det) != matrix0DetActual) {
        printf("matrix 4 failed\nActual matrix 0 Determinant %f\n\n", matrix4DetActual);
        failCount++;
    } else printf("Pass\n");
    
    if (failCount == 0) {
        printf("Determinant Float Passed all Tests\n");
    } else {
        printf("Determinant Float Failed %d Tests\n", failCount);
    }
    deleteMatrixFloat(matrix0);
    deleteMatrixFloat(matrix1);
    deleteMatrixFloat(matrix2);
    deleteMatrixFloat(matrix3);
    deleteMatrixFloat(matrix4);
}

void testDeterminant2DInt() {
    int failCount = 0;
    
    MatrixInt *matrix0 = createMatrixZerosInt(3, 3);
    MatrixInt *matrix1 = createMatrixOnesInt(3, 3);
    MatrixInt *matrix2 = createMatrixInt(4, 4);
    MatrixInt *matrix3 = createMatrixInt(6, 6);
    MatrixInt *matrix4 = createMatrixInt(6, 6);
    
    int matrix0DetActual = 0;
    int matrix1DetActual = 0;
    
    memcpy(matrix2->contents, (int [16]){   1,9,7,1,
                                            4,1,5,1,
                                            1,6,1,1,
                                            2,1,7,3}, 16*sizeof(int));
    int matrix2DetActual = 384;
    
    memcpy(matrix3->contents, (int [36]){   5,2,9,1,5,8,
                                            4,2,8,8,7,5,
                                            7,4,5,6,8,6,
                                            3,9,2,7,4,0,
                                            0,1,3,2,1,3,
                                            8,4,5,5,6,9}, 36*sizeof(int));
    int matrix3DetActual = 10769;
    
    memcpy(matrix4->contents, (int [36]){   5,2,9,-1,5,8,
                                            4,2,8,8,7,5,
                                            7,4,5,6,8,6,
                                            3,9,2,7,4,0,
                                            0,1,3,2,1,3,
                                            8,4,-5,5,6,9}, 36*sizeof(int));
    int matrix4DetActual = 3343;
    
    printf("\ntesting matrix 0\n");
    int matrix0Det;
    determinant2DInt(matrix0, &matrix0Det, queue);
    printf("matrix0Det = %d\n", matrix0Det);
    if (matrix0Det != matrix0DetActual) {
        printf("matrix 0 failed\nActual matrix 0 Determinant %d\n\n", matrix0DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 1\n");
    int matrix1Det;
    determinant2DInt(matrix1, &matrix1Det, queue);
    printf("matrix1Det = %d\n", matrix1Det);
    if (matrix1Det != matrix1DetActual) {
        printf("matrix 1 failed\nActual matrix 0 Determinant %d\n\n", matrix1DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 2\n");
    int matrix2Det;
    determinant2DInt(matrix2, &matrix2Det, queue);
    printf("matrix2Det = %d\n", matrix2Det);
    if (matrix2Det != matrix2DetActual) {
        printf("matrix 2 failed\nActual matrix 0 Determinant %d\n\n", matrix2DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 3\n");
    int matrix3Det;
    determinant2DInt(matrix3, &matrix3Det, queue);
    printf("matrix3Det = %d\n", matrix3Det);
    if (matrix3Det != matrix3DetActual) {
        printf("matrix 3 failed\nActual matrix 0 Determinant %d\n\n", matrix3DetActual);
        failCount++;
    } else printf("Pass\n");
    
    printf("\ntesting matrix 4\n");
    int matrix4Det;
    determinant2DInt(matrix4, &matrix4Det, queue);
    printf("matrix4Det = %d\n", matrix4Det);
    if (matrix4Det != matrix4DetActual) {
        printf("matrix 4 failed\nActual matrix 0 Determinant %d\n\n", matrix4DetActual);
        failCount++;
    } else printf("Pass\n");
    
    if (failCount == 0) {
        printf("Determinant Int Passed all Tests\n");
    } else {
        printf("Determinant Int Failed %d Tests\n", failCount);
    }
    deleteMatrixInt(matrix0);
    deleteMatrixInt(matrix1);
    deleteMatrixInt(matrix2);
    deleteMatrixInt(matrix3);
    deleteMatrixInt(matrix4);
}

void testDeterminantChio2DFloat() {
    
}

void testDeterminantChio2DInt() {
    
}

#pragma mark Helper Function Tests

void testCreateRange1D() {
    
}

void testCreateRangeSquare2D() {
    
}

void testCreateRange2D() {
    
}
