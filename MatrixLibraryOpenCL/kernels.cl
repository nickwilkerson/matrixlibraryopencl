kernel void elementMultiply1DInt(
                                 global int *input1,
                                 global int *input2,
                                 global int *output)
{
    size_t i = get_global_id(0);
    output[i] = input1[i] * input2[i];
}

kernel void elementMultiply1DFloat (
                              global float *input1,
                              global float *input2,
                              global float *output)
{
    size_t i = get_global_id(0);
    output[i] = input1[i] * input2[i];
}

kernel void scalarAdd2DFloat(
                                global float *inputMatrix,
                                global float *scalar,
                                global float *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    outputMatrix[width * row + column] = inputMatrix[width * row + column] + *scalar;
}

kernel void scalarAdd2DInt(
                             global int *inputMatrix,
                             global int *scalar,
                             global int *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    outputMatrix[width * row + column] = inputMatrix[width * row + column] + *scalar;
}

kernel void scalarMultiply1DFloat(
                                  global float *arrayInput,
                                  global float *scalar,
                                  global float *arrayResult)
{
    size_t index = get_global_id(0);
    arrayResult[index] = arrayInput[index] * (*scalar);
}

kernel void scalarMultiply1DInt(
                                  global int *arrayInput,
                                  global int *scalar,
                                  global int *arrayResult)
{
    size_t index = get_global_id(0);
    arrayResult[index] = arrayInput[index] * (*scalar);
}

kernel void scalarMultiply2DFloat(
                             global float *inputMatrix,
                             global float *scalarPtr,
                             global float *outputMatrix)
{
//    printf("1\n");
    size_t column = get_global_id(0);
//    printf("2\n");
    size_t row = get_global_id(1);
//    printf("3\n");
    size_t width = get_global_size(0);
//    printf("4\n");
    float scalar = *scalarPtr;
//    printf("scalar = %f\n", scalar);
    outputMatrix[width * row + column] = inputMatrix[width * row + column] * scalar;
}

kernel void scalarMultiply2DInt(
                           global int *inputMatrix,
                           global int *scalar,
                           global int *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    outputMatrix[width * row + column] = inputMatrix[width * row + column] * *scalar;
}


kernel void add1DInt(
                         global int *input1,
                         global int *input2,
                         global int *output)
{
     size_t i = get_global_id(0);
     output[i] = input1[i] + input2[i];
}
 
kernel void add1DFloat(
                          global float *input1,
                          global float *input2,
                          global float *output)
{
     size_t i = get_global_id(0);
     output[i] = input1[i] + input2[i];
}

kernel void transpose2DFloat(
                                global float *inputMatrix,
                                global float *outputMatrix
)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    outputMatrix[width * row + column] = inputMatrix[height * column + row];
}

kernel void transpose2DInt(
                                global int *inputMatrix,
                                global int *outputMatrix
)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    outputMatrix[width * row + column] = inputMatrix[height * column + row];
}

kernel void sumRows2DFloat(
                            global float *inputMatrix,
                            global int *matrixWidthPtr,
                            global float *outputArray)
{

    size_t row = get_global_id(0);
    size_t width = *matrixWidthPtr;
    float sum = 0;
    for (int column = 0; column < width; column++) {
        sum += inputMatrix[width * row + column];
    }
    outputArray[row] = sum;
}

kernel void sumRows2DInt(
                           global int *inputMatrix,
                           global int *matrixWidthPtr,
                           global int *outputArray)
{
    size_t row = get_global_id(0);
    size_t width = *matrixWidthPtr;
    int sum = 0;
    for (int column = 0; column < width; column++) {
        sum += inputMatrix[width * row + column];
    }
    outputArray[row] = sum;
}

kernel void sumColumns2DFloat(
                           global float *inputMatrix,
                           global int *matrixHeightPtr,
                           global float *outputArray)
{
    
    size_t column = get_global_id(0);
    size_t width = get_global_size(0);
    size_t height = *matrixHeightPtr;
    float sum = 0;
    for (int row = 0; row < height; row++) {
        sum += inputMatrix[width * row + column];
    }
    outputArray[column] = sum;
}

kernel void sumColumns2DInt(
                         global int *inputMatrix,
                         global int *matrixHeightPtr,
                         global int *outputArray)
{
    size_t column = get_global_id(0);
    size_t width = get_global_size(0);
    size_t height = *matrixHeightPtr;
    float sum = 0;
    for (int row = 0; row < height; row++) {
        sum += inputMatrix[width * row + column];
    }
    outputArray[column] = sum;
}

kernel void vectorColumnAddToMatrixFloat(
                        global float *inputVector,
                        global float *inputMatrix,
                        global float *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    
    outputMatrix[width * row + column] = inputMatrix[width * row + column] + inputVector[row];
}

kernel void vectorColumnAddToMatrixInt(
                                         global int *inputVector,
                                         global int *inputMatrix,
                                         global int *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    
    outputMatrix[width * row + column] = inputMatrix[width * row + column] + inputVector[row];
}

kernel void vectorRowAddToMatrixFloat(
                                         global float *inputVector,
                                         global float *inputMatrix,
                                         global float *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    
    outputMatrix[width * row + column] = inputMatrix[width * row + column] + inputVector[column];
}

kernel void vectorRowAddToMatrixInt(
                                       global int *inputVector,
                                       global int *inputMatrix,
                                       global int *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    
    outputMatrix[width * row + column] = inputMatrix[width * row + column] + inputVector[column];
}

kernel void activate2DFloat(global float *inputMatrix,
                            global float *outputMatrix)
{
    size_t column = get_global_id(0);
    size_t row = get_global_id(1);
    size_t width = get_global_size(0);
    outputMatrix[width * row + column] = 1/(1 + exp(-1 * inputMatrix[width * row + column]));
}

kernel void add2DFloat(global float *input1,
                       global float *input2,
                       global float *output)
{
    int column = get_global_id(0);
    int row = get_global_id(1);
    int width = get_global_size(0);
    output[width * row + column] = input1[width * row + column] + input2[width * row + column];
}

kernel void add2DInt(global int *input1,
                       global int *input2,
                       global int *output)
{
    int column = get_global_id(0);
    int row = get_global_id(1);
    int width = get_global_size(0);
    output[width * row + column] = input1[width * row + column] + input2[width * row + column];
}

kernel void subtract2DFloat(global float *input1,
                       global float *input2,
                       global float *output)
{
    int column = get_global_id(0);
    int row = get_global_id(1);
    int width = get_global_size(0);
    output[width * row + column] = input1[width * row + column] - input2[width * row + column];
}

kernel void subtract2DInt(global int *input1,
                     global int *input2,
                     global int *output)
{
    int column = get_global_id(0);
    int row = get_global_id(1);
    int width = get_global_size(0);
    output[width * row + column] = input1[width * row + column] - input2[width * row + column];
}

kernel void elementMultiply2DFloat(global float *input1,
                            global float *input2,
                            global float *output)
{
    int column = get_global_id(0);
    int row = get_global_id(1);
    int width = get_global_size(0);
    output[width * row + column] = input1[width * row + column] * input2[width * row + column];
}

kernel void elementMultiply2DInt(global int *input1,
                          global int *input2,
                          global int *output)
{
    int column = get_global_id(0);
    int row = get_global_id(1);
    int width = get_global_size(0);
    output[width * row + column] = input1[width * row + column] * input2[width * row + column];
}

kernel void multiply2DFloat(global float *input1,
                          global float *input2,
                          global float *output,
                          global int *innerSizePtr) {
    int width = get_global_size(0);
    int height = get_global_size(1);
    int height1 = height;
    int width2 = width;
    int column = get_global_id(0);
    int row = get_global_id(1);
    int innerSize = *innerSizePtr;
//    printf("innerSize = %d\n", innerSize);
    int height2 = innerSize;
    int width1 = innerSize;
    output[width * row + column] = 0;
    for (int index = 0; index < innerSize; index++)
    {
        int column1 = index;
        int row2 = index;
        int column2 = column;//check this
        int row1 = row;//check this
        output[width * row + column] += input1[width1 * row1 + column1] * input2[width2 * row2 + column2];
    }
}

kernel void multiply2DInt(global int *input1,
                            global int *input2,
                            global int *output,
                            global int *innerSizePtr) {
    int width = get_global_size(0);
    int height = get_global_size(1);
    int height1 = height;
    int width2 = width;
    int column = get_global_id(0);
    int row = get_global_id(1);
    int innerSize = *innerSizePtr;
    //    printf("innerSize = %d\n", innerSize);
    int height2 = innerSize;
    int width1 = innerSize;
    output[width * row + column] = 0;
    for (int index = 0; index < innerSize; index++)
    {
        int column1 = index;
        int row2 = index;
        int column2 = column;//check this
        int row1 = row;//check this
        output[width * row + column] += input1[width1 * row1 + column1] * input2[width2 * row2 + column2];
    }
}

/*
 *  det(cA)=cndet(A)
 */
kernel void determinantChio2DFloat(global float *matrixIn,
                                   global float *matrixOut,
                                   global int *indexWidthPtr
                                   ) {//width of actual memory
    int indexWidth = *indexWidthPtr;
    int widthOut = get_global_size(0);
    int heightOut = get_global_size(1);
    int widthIn = widthOut + 1;
    int heightIn = heightOut + 1;
    int column = get_global_id(0);
    int row = get_global_id(1);
    
//    printf("%f * %f - %f * %f = %f\n", matrixIn[0], matrixIn[indexWidth * (row + 1) + (column + 1)],
//           matrixIn[indexWidth * (row + 1) + (0)], matrixIn[(column + 1)], (matrixIn[0] * matrixIn[indexWidth * (row + 1) + (column + 1)] -
//                                                                            (matrixIn[indexWidth * (row + 1) + (0)] * matrixIn[(column + 1)])));
    
    float det = (matrixIn[0] * matrixIn[indexWidth * (row + 1) + (column + 1)] -
                 (matrixIn[indexWidth * (row + 1) + (0)] * matrixIn[(column + 1)]));
    if (det != det) {
        printf("found a nan at size %d row %d column %d\n", widthOut, row, column);
        printf("upper left before = %f\n", matrixIn[0]);
    }
    matrixOut[indexWidth * row + column] = det;
        

}

/*
 *  det(cA)=cndet(A)
 */
kernel void determinantChio2DInt(global int *matrixIn,
                                   global int *matrixOut,
                                   global int *indexWidthPtr
                                   ) {//width of actual memory
    int indexWidth = *indexWidthPtr;
    int widthOut = get_global_size(0);
    int heightOut = get_global_size(1);
    int widthIn = widthOut + 1;
    int heightIn = heightOut + 1;
    int column = get_global_id(0);
    int row = get_global_id(1);
    
//    printf("%f * %f - %f * %f\n", matrixIn[0], matrixIn[indexWidth * (row + 1) + (column + 1)],
 //          matrixIn[indexWidth * (row + 1) + (0)], matrixIn[(column + 1)]);
    int det = (matrixIn[0] * matrixIn[indexWidth * (row + 1) + (column + 1)] -
                 (matrixIn[indexWidth * (row + 1) + (0)] * matrixIn[(column + 1)]));
    if (det != det) {
        printf("found a nan at size %d row %d column %d\n", widthOut, row, column);
        printf("upper left before = %f\n", matrixIn[0]);
    }
    matrixOut[indexWidth * row + column] = det;
    
    
}

/*
kernel void copyLastRowForDetFloat(global float *matrix,
                             global int *heightPtr,
                             global float *innerDetMatrix) {
    int width = get_global_size(0);
    int height = *heightPtr;
    int column = get_global_id(0);
    int row = height - 1;
    innerDetMatrix[width * row + column] = matrix[width * row + column];
}
*/





