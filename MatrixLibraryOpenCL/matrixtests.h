//
//  matrixtests.h
//  ANN
//
//  Created by Nicholas Wilkerson and Dave Heyborne on 12/21/14.
//  Copyright (c) 2014 Nicholas Wilkerson and Dave Heyborne. All rights reserved.
//

#pragma once

#include <stdio.h>
#include "matrix.h"
#include <math.h>

void runMatrixTests();

#pragma mark Vector Creation Tests

void testCreateVectorFloat();
void testCreateVectorInt();
void testCreateVectorZerosFloat();
void testCreateVectorZerosInt();
void testCreateVectorOnesFloat();
void testCreateVectorOnesInt();
void testSetAllVectorFloatValuesTo();
void testSetAllVectorIntValuesTo();

#pragma mark Matrix Creation Tests

void testCreateMatrixFloat();
void testCreateMatrixInt();
void testCreateMatrixZerosFloat();
void testCreateMatrixZerosInt();
void testCreateMatrixOnesFloat();
void testCreateMatrixOnesInt();
void testCreateMatrixFloatFromVector();
void testCreateMatrixIntFromVector();
void testSetAllMatrixFloatValuesTo();
void testSetAllMatrixIntValuesTo();

#pragma mark Vector Deletion Tests

void testDeleteVectorFloat();
void testDeleteVectorInt();

#pragma mark Matrix Deletion Tests

void testDeleteMatrixFloat();
void testDeleteMatrixInt();

#pragma mark Vector Printing Tests

void testPrintVectorFloat();
void testPrintVectorInt();

#pragma mark Matrix Printing Tests

void testPrintMatrixFloat();
void testPrintMatrixInt();
void testPrintUpperLeftSquareFloat();
void testPrintUpperLeftSquareInt();

#pragma mark Vector-Only Function Tests

void testTranspose1DFloat();
void testTranspose1DInt();
void testDotFloat();
void testScalarMultiply1DFloat();
void testScalarMultiply1DInt();
void testElementMultiply1DFloat();
void testElementMultiply1DInt();

#pragma mark Matrix Functions - One Matrix Tests

void testTranspose2DFloat();
void testTranspose2DInt();
void testElementSum2DFloat();
void testElementSum2DInt();

#pragma mark Matrix Functions - Scalar Tests

void testScalarAdd2DFloat();
void testScalarAdd2DInt();
void testScalarMultiply2DFloat();
void testScalarMultiply2DInt();

#pragma mark Matrix Functions - Per-Element Tests

void testAdd2DFloat();
void testAdd2DInt();
void testSubtract2DFloat();
void testSubtract2DInt();
void testElementMultiply2DFloat();
void testElementMultiply2DInt();

#pragma mark Matrix Functions - Vector Tests

void testSumRows2DFloat();
void testSumRows2DInt();
void testSumColumns2DFloat();
void testSumColumns2DInt();
void testVectorColumnAddToMatrixFloat();
void testVectorRowAddToMatrixFloat();

#pragma mark Matrix Functions - Two Matrices Tests

void testMultiply2DFloat();
void testMultiply2DFloat_CPU();
void testMultiply2DInt();

#pragma mark Matrix Append Function Tests

void testAppendHorizontalFloat();
void testAppendHorizontalInt();
void testAppendVerticalFloat();
void testAppendVerticalInt();

#pragma mark Determinant Function Tests

void testDeterminant2DFloat();
void testDeterminant2DInt();
void testDeterminantChio2DFloat();
void testDeterminantChio2DInt();

#pragma mark Helper Function Tests

void testCreateRange1D();
void testCreateRangeSquare2D();
void testCreateRange2D();
