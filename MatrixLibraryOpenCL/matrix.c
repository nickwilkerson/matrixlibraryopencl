//
//  matrix.c
//  ANN
//
//  Created by Nicholas Wilkerson and Dave Heyborne on 12/21/14.
//  Copyright (c) 2014 Nicholas Wilkerson and Dave Heyborne. All rights reserved.
//

#include <math.h>
#include "matrix.h"

#pragma mark OpenCL Initialization

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

dispatch_queue_t openCLInit() {
    char name[128];
    
    cl_uint num = 0;
    clGetDeviceIDs(NULL,CL_DEVICE_TYPE_GPU,0,NULL,&num);
    
    dispatch_queue_t queue = NULL;
    if (num > 0) {
        cl_device_id devices[num];
        clGetDeviceIDs(NULL,CL_DEVICE_TYPE_GPU,num,devices,NULL);
        
        cl_device_id deviceId = NULL;
        for (int i = 0; i < num; i++) {
            deviceId = devices[i];
            char deviceName[128];
            clGetDeviceInfo(deviceId, CL_DEVICE_NAME, 128, deviceName, NULL);
            fprintf(stdout, "Found %s\n", deviceName);
        }
    
        queue = gcl_create_dispatch_queue(0/*CL_DEVICE_TYPE_GPU*/, deviceId);
    }
    // In the event that our system does NOT have an OpenCL-compatible GPU,
    // we can use the OpenCL CPU compute device instead.
    if (queue == NULL) {
        printf("No compatible GPU using CPU instead\n");
        queue = gcl_create_dispatch_queue(CL_DEVICE_TYPE_CPU, NULL);
    }
    
    cl_device_id gpu = gcl_get_device_id_with_dispatch_queue(queue);
    clGetDeviceInfo(gpu, CL_DEVICE_NAME, 128, name, NULL);
    fprintf(stdout, "Created a dispatch queue using the %s\n", name);
    return queue;
}

#pragma mark Vector Creation

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

MatrixFloat *createVectorFloat(INT size, VectorOrientation orientation) {
    float *contents = (float *)malloc(sizeof(float) * size);
    MatrixFloat *vector = (MatrixFloat *)malloc(sizeof(MatrixFloat));
    vector->contents = contents;
    if (orientation == ROW) {
        vector->width = size;
        vector->height = 1;
    } else if (orientation == COLUMN) {
        vector->width = 1;
        vector->height = size;
    }
    return vector;
}

MatrixInt *createVectorInt(INT size, VectorOrientation orientation) {
    int *contents = (int *)malloc(sizeof(int) * size);
    MatrixInt *vector = (MatrixInt *)malloc(sizeof(MatrixInt));
    vector->contents = contents;
    if (orientation == ROW) {
        vector->width = size;
        vector->height = 1;
    } else if (orientation == COLUMN) {
        vector->width = 1;
        vector->height = size;
    }
    return vector;
}

MatrixFloat *createVectorZerosFloat(INT size, VectorOrientation orientation) {
    float *contents = (float *)calloc(sizeof(float), size);
    MatrixFloat *vector = (MatrixFloat *)malloc(sizeof(MatrixFloat));
    vector->contents = contents;
    if (orientation == ROW) {
        vector->width = size;
        vector->height = 1;
    } else if (orientation == COLUMN) {
        vector->width = 1;
        vector->height = size;
    }
    return vector;
}

MatrixInt *createVectorZerosInt(INT size, VectorOrientation orientation) {
    int *contents = (int *)calloc(sizeof(int), size);
    MatrixInt *vector = (MatrixInt *)malloc(sizeof(MatrixInt));
    vector->contents = contents;
    if (orientation == ROW) {
        vector->width = size;
        vector->height = 1;
    } else if (orientation == COLUMN) {
        vector->width = 1;
        vector->height = size;
    }
    return vector;
}

MatrixFloat *createVectorOnesFloat(INT size, VectorOrientation orientation) {
    MatrixFloat *vector = createVectorFloat(size, orientation);
    setAllVectorFloatValuesTo(vector, 1.0);
    return vector;
}

MatrixInt *createVectorOnesInt(INT size, VectorOrientation orientation) {
    MatrixInt *vector = createVectorInt(size, orientation);
    setAllVectorIntValuesTo(vector, 1);
    return vector;
}

int setAllVectorFloatValuesTo(MatrixFloat *vector, FLOAT value) {
    if (vector->height != 1 && vector->width != 1) {
        printf("setAllVectorFloatValuesTo: input not a vector\n");
        return -1;
    }
    for (int i = 0; i < vector->width * vector->height; ++i) {
        vector->contents[i] = value;
    }
    return 0;
}

int setAllVectorIntValuesTo(MatrixInt *vector, INT value) {
    if (vector->height != 1 && vector->width != 1) {
        printf("setAllVectorFloatValuesTo: input not a vector\n");
        return -1;
    }
    for (int i = 0; i < vector->width *vector->height; ++i) {
        vector->contents[i] = value;
    }
    return 0;
}

#pragma mark Matrix Creation

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

MatrixFloat *createMatrixFloat(INT width, INT height) {
    float *contents = (float *)malloc(sizeof(float) * width * height);
    MatrixFloat *matrix = (MatrixFloat *)malloc(sizeof(MatrixFloat));
    matrix->contents = contents;
    matrix->width = width;
    matrix->height = height;
    return matrix;
}

MatrixInt *createMatrixInt(INT width, INT height) {
    int *contents = (int *)malloc(sizeof(int) * width * height);
    MatrixInt *matrix = (MatrixInt *)malloc(sizeof(MatrixInt));
    matrix->contents = contents;
    matrix->width = width;
    matrix->height = height;
    return matrix;
}


MatrixFloat *createMatrixZerosFloat(INT width, INT height) {
    float *contents = (float *)calloc(sizeof(float), width * height);
    MatrixFloat *matrix = (MatrixFloat *)malloc(sizeof(MatrixFloat));
    matrix->contents = contents;
    matrix->width = width;
    matrix->height = height;
    return matrix;
}

MatrixInt *createMatrixZerosInt(INT width, INT height) {
    int *contents = (int *)calloc(sizeof(int), width * height);
    MatrixInt *matrix = (MatrixInt *)malloc(sizeof(MatrixInt));
    matrix->contents = contents;
    matrix->width = width;
    matrix->height = height;
    return matrix;
}

MatrixFloat *createMatrixOnesFloat(INT width, INT height) {
    MatrixFloat *matrix = createMatrixFloat(width, height);
    setAllMatrixFloatValuesTo(matrix, 1.0);
    return matrix;
}

MatrixInt *createMatrixOnesInt(INT width, INT height) {
    MatrixInt *matrix = createMatrixInt(width, height);
    setAllMatrixIntValuesTo(matrix, 1);
    return matrix;
}
/*
MatrixFloat *createMatrixFloatFromVector(MatrixFloat *vector) {
    float *contents = (float *)calloc(sizeof(float), vector->size);
    MatrixFloat *matrix = (MatrixFloat *)malloc(sizeof(MatrixFloat));
    matrix->contents = contents;
    if (vector->orientation == ROW) {
        matrix->width = vector->size;
        matrix->height = 1;
    } else {
        matrix->width = 1;
        matrix->height = vector->size;
    }
    memcpy(matrix->contents, vector->contents, sizeof(float) * vector->size);
    return matrix;
}

MatrixInt *createMatrixIntFromVector(MatrixInt *vector) {
    int *contents = (int *)calloc(sizeof(int), vector->size);
    MatrixInt *matrix = (MatrixInt *)malloc(sizeof(MatrixInt));
    matrix->contents = contents;
    if (vector->orientation == ROW) {
        matrix->width = vector->size;
        matrix->height = 1;
    } else {
        matrix->width = 1;
        matrix->height = vector->size;
    }
    memcpy(matrix->contents, vector->contents, sizeof(int) * vector->size);
    return matrix;
}
*/
void setAllMatrixFloatValuesTo(MatrixFloat *matrix, FLOAT value) {
    int matrixSize = (matrix->width * matrix->height);
    for (int i = 0; i < matrixSize; ++i) {
        matrix->contents[i] = value;
    }
}

void setAllMatrixIntValuesTo(MatrixInt *matrix, INT value) {
    int matrixSize = (matrix->width * matrix->height);
    for (int i = 0; i < matrixSize; ++i) {
        matrix->contents[i] = value;
    }
}

#pragma mark Vector Deletion

/********************************************************************************
 *
 *
 *
 ********************************************************************************/
/*
int deleteVectorFloat(VectorFloat *vector) {
    int error = 0;
    free(vector->contents);
    free(vector);
    return error;
}

int deleteVectorInt(VectorInt *vector) {
    int error = 0;
    free(vector->contents);
    free(vector);
    return error;
}
*/
#pragma mark Matrix Deletion

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int deleteMatrixFloat(MatrixFloat *matrix) {
    int error = 0;
    free(matrix->contents);
    free(matrix);
    return error;
}

int deleteMatrixInt(MatrixInt *matrix) {
    int error = 0;
    free(matrix->contents);
    free(matrix);
    return error;
}

#pragma mark Vector Printing

/********************************************************************************
 *
 *
 *
 ********************************************************************************/
/*
void printVectorFloat(VectorFloat *vector) {
    printf("\n");
    for (int i=0; i < vector->size; i++) {
        printf("%.1f ", vector->contents[i]);
        if (vector->orientation == ROW) {
            printf(" ");
        } else {
            printf("\n");
        }
    }
    printf("\n");
}

void printVectorInt(VectorInt *vector) {
    printf("\n");
    for (int i=0; i < vector->size; i++) {
        printf("%d", vector->contents[i]);
        if (vector->orientation == ROW) {
            printf(" ");
        } else {
            printf("\n");
        }
    }
    printf("\n");
}
*/
#pragma mark Matrix Printing

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

void printMatrixFloat(MatrixFloat *matrix) {
    for (int row=0; row < matrix->height; row++) {
        for (int column=0; column < matrix->width; column++) {
            printf("%.1f ", matrix->contents[row * matrix->width + column]);
        }
        printf("\n");
    }
    printf("\n");
}

void printMatrixInt(MatrixInt *matrix) {
    for (int row=0; row < matrix->height; row++) {
        for (int column=0; column < matrix->width; column++) {
            printf("%d ", matrix->contents[row * matrix->width + column]);
        }
        printf("\n");
    }
    printf("\n");
}

void printUpperLeftSquareFloat(MatrixFloat *matrix, INT size) {
    for (int row=0; row < size; row++) {
        for (int column=0; column < size; column++) {
            printf("%.1f ", matrix->contents[row * matrix->width + column]);
        }
        printf("\n");
    }
    printf("\n");
}

void printUpperLeftSquareInt(MatrixInt *matrix, INT size) {
    for (int row=0; row < size; row++) {
        for (int column=0; column < size; column++) {
            printf("%d ", matrix->contents[row * matrix->width + column]);
        }
        printf("\n");
    }
    printf("\n");
}

#pragma mark Vector-Only Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int transpose1DFloat(MatrixFloat *vector) {
    if (vector->width != 1 && vector->height != 1) {
        printf("transpose1DFloat: input not a vector\n");
        return -1;
    }
    INT initialWidth = vector->width;
    INT initialHeight = vector->height;
    vector->width = initialHeight;
    vector->height = initialWidth;
    return 0;
}

int transpose1DInt(MatrixInt *vector) {
    if (vector->width != 1 && vector->height != 1) {
        printf("transpose1DFloat: input not a vector\n");
        return -1;
    }
    INT initialWidth = vector->width;
    INT initialHeight = vector->height;
    vector->width = initialHeight;
    vector->height = initialWidth;
    return 0;
}

int dotFloat(MatrixFloat *input1, MatrixFloat *input2, FLOAT *result, dispatch_queue_t queue) {
    int error = 0;
    if ((input1->width != 1 && input1->height != 1) || (input1->width != 1 && input1->height != 1)) {
        printf("dotFloat: inputs must be vectors\n");
        return -1;
    } else if (input1->width * input1->height != input2->width * input2->height) {
        printf("dotFloat: input sizes must match\n");
        return -1;
    }
    MatrixFloat *outputVector = createVectorFloat(input1->width * input1->height, ROW);

    error = elementMultiply1DFloat(input1, input2, outputVector, queue);
    float sum = 0;
    for (int i = 0; i < input1->width * input1->height; i++) {
        sum = sum + outputVector->contents[i];
    }
    *result = sum;
    deleteMatrixFloat(outputVector);
    return error;
}

int scalarMultiply1DFloat(MatrixFloat *vectorInput, FLOAT scalar, MatrixFloat *vectorResult, dispatch_queue_t queue) {
    if ((vectorInput->width != 1 && vectorInput->height != 1) || (vectorResult->width != 1 && vectorResult->height != 1)) {
        printf("scalarMultiply1DFloat: inputs must be vectors\n");
        return -1;
    } else if (vectorResult->width * vectorResult->height != vectorInput->width * vectorInput->height) {
        printf("matrix height must equal vector size and matrixInput must have same dimensions as matrixOutput\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_float) * vectorInput->width * vectorInput->height, vectorInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in_scalar  = gcl_malloc(sizeof(cl_float), &scalar,
                                      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * vectorResult->width * vectorResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(vectorResult->width * vectorResult->height, scalarMultiply1DFloat_kernel);
        
        scalarMultiply1DFloat_kernel(&range,(cl_float*)mem_in, (cl_float*)mem_in_scalar, (cl_float*)mem_out);
        gcl_memcpy(vectorResult->contents, mem_out, sizeof(cl_float) * vectorResult->width * vectorResult->height);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_in_scalar);
    gcl_free(mem_out);
    
    return 0;
}

int scalarMultiply1DInt(MatrixInt *vectorInput, INT scalar, MatrixInt *vectorResult, dispatch_queue_t queue) {
    if ((vectorInput->width != 1 && vectorInput->height != 1) || (vectorResult->width != 1 && vectorResult->height != 1)) {
        printf("scalarMultiply1DInt: inputs must be vectors\n");
        return -1;
    } else if (vectorResult->width * vectorResult->height != vectorInput->width * vectorInput->height) {
        printf("matrix height must equal vector size and matrixInput must have same dimensions as matrixOutput\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_int) * vectorInput->width * vectorInput->height, vectorInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in_scalar  = gcl_malloc(sizeof(cl_int), &scalar,
                                      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_int) * vectorResult->width * vectorResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(vectorResult->width * vectorResult->height, scalarMultiply1DInt_kernel);
        
        scalarMultiply1DInt_kernel(&range,(cl_int*)mem_in, (cl_int*)mem_in_scalar, (cl_int*)mem_out);
        gcl_memcpy(vectorResult->contents, mem_out, sizeof(cl_int) * vectorResult->width * vectorResult->height);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_in_scalar);
    gcl_free(mem_out);
    return 0;
}

int elementMultiply1DFloat(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *output, dispatch_queue_t queue) {

    if ((input1->width != 1 && input1->height != 1) || (input2->width != 1 && input2->height != 1)) {
        printf("elementMultiply1DFloat: inputs must be vectors\n");
        return -1;
    } else if (output->width != 1 && output->height != 1) {
        printf("elementMultiply1DFloat: output must be a vector\n");
        return -1;
    } else if (input1->width * input1->height != input2->width * input2->height) {
        printf("elementMultiply1DFloat: input sizes must match\n");
        return -1;
    }
    if (input1->width * input1->height != output->width * output->height) {
        printf("elementMultiply1DFloat: output size must match input size\n");
        return -1;
    }
    void* mem_in1 = gcl_malloc(sizeof(cl_float) * input1->width * input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2 = gcl_malloc(sizeof(cl_float) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * output->width * output->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(output->width * output->height, elementMultiply1DFloat_kernel);
        
        elementMultiply1DFloat_kernel(&range,(cl_float*)mem_in1, (cl_float*)mem_in2, (cl_float*)mem_out);
        gcl_memcpy(output->contents, mem_out, sizeof(cl_float) * output->width * output->height);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return 0;
}

int elementMultiply1DInt(MatrixInt *input1, MatrixInt *input2, MatrixInt *output, dispatch_queue_t queue) {
    if ((input1->width != 1 && input1->height != 1) || (input2->width != 1 && input2->height != 1)) {
        printf("elementMultiply1DInt: inputs must be vectors\n");
        return -1;
    } else if (output->width != 1 && output->height != 1) {
        printf("elementMultiply1DInt: output must be a vector\n");
        return -1;
    } else if (input1->width * input1->height != input2->width * input2->height) {
        printf("elementMultiply1DInt: input sizes must match\n");
        return -1;
    }
    if (input1->width * input1->height != output->width * output->height) {
        printf("elementMultiply1DInt: output size must match input size\n");
        return -1;
    }
    void* mem_in1  = gcl_malloc(sizeof(cl_float) * input1->width * input1->height, input1,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_float) * input2->width * input2->height, input2,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * input1->width * input1->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(output->width * output->height, elementMultiply1DInt_kernel);
        
        elementMultiply1DInt_kernel(&range,(cl_int*)mem_in1, (cl_int*)mem_in2, (cl_int*)mem_out);
        gcl_memcpy(output->contents, mem_out, sizeof(cl_int) * input1->width * input1->height);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return 0;
}


#pragma mark Matrix Functions - One Matrix

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int transpose2DFloat(MatrixFloat *matrixInput, MatrixFloat *matrixResult, dispatch_queue_t queue) {
    if (matrixInput->height != matrixResult->width) {
        printf("input matrix height must equal output matrix width\n");
        return -1;
    }
    if (matrixInput->width != matrixResult->height) {
        printf("input matrix width must equal output matrix height\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixInput->width, matrixInput->height, transpose2DFloat_kernel);
        
        transpose2DFloat_kernel(&range,(cl_float*)mem_in, (cl_float*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_float) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_out);
    return 0;
}

int transpose2DInt(MatrixInt *matrixInput, MatrixInt *matrixResult, dispatch_queue_t queue) {
    if (matrixInput->height != matrixResult->width) {
        printf("input matrix height must equal output matrix width\n");
        return -1;
    }
    if (matrixInput->width != matrixResult->height) {
        printf("input matrix width must equal output matrix height\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_int) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixInput->width, matrixInput->height, transpose2DInt_kernel);
        
        transpose2DInt_kernel(&range,(cl_int*)mem_in, (cl_int*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_int) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_out);
    return 0;
}

float elementSum2DFloat(MatrixFloat *matrix, dispatch_queue_t queue) {
    MatrixFloat *sumVector = createVectorFloat(matrix->height, COLUMN);
    sumRows2DFloat(matrix, sumVector, queue);
    float sum = 0;
    for (int i = 0; i < sumVector->width * sumVector->height; i++) {
        sum += sumVector->contents[i];
    }
    deleteMatrixFloat(sumVector);
    return sum;
}

int elementSum2DInt(MatrixInt *matrix, dispatch_queue_t queue) {
    MatrixInt *sumVector = createVectorInt(matrix->height, COLUMN);
    sumRows2DInt(matrix, sumVector, queue);
    int sum = 0;
    for (int i = 0; i < sumVector->width * sumVector->height; i++) {
        sum += sumVector->contents[i];
    }
    deleteMatrixInt(sumVector);
    return sum;
}

#pragma mark Matrix Functions - Scalars

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int scalarAdd2DFloat(MatrixFloat *matrixInput, FLOAT scalar, MatrixFloat *matrixResult, dispatch_queue_t queue) {
    int error = 0;
    if (matrixResult->height != matrixInput->height || matrixResult->width != matrixInput->width) {
        printf("scalarAdd2DFloat: result matrix dimensions must match input\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in_scalar  = gcl_malloc(sizeof(cl_float), &scalar,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixInput->width, matrixInput->height, scalarAdd2DFloat_kernel);
        
        scalarAdd2DFloat_kernel(&range,(cl_float*)mem_in, (cl_float*)mem_in_scalar, (cl_float*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_float) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_in_scalar);
    gcl_free(mem_out);
    
    return error;
}

int scalarAdd2DInt(MatrixInt *matrixInput, INT scalar, MatrixInt *matrixResult, dispatch_queue_t queue) {
    int error = 0;
    if (matrixResult->height != matrixInput->height || matrixResult->width != matrixInput->width) {
        printf("scalarAdd2DInt: result matrix dimensions must match input\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_int) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in_scalar  = gcl_malloc(sizeof(cl_int), &scalar,
                                      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixInput->width, matrixInput->height, scalarAdd2DInt_kernel);
        
        scalarAdd2DInt_kernel(&range,(cl_int*)mem_in, (cl_int*)mem_in_scalar, (cl_int*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_int) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_in_scalar);
    gcl_free(mem_out);
    
    return error;
}

int scalarMultiply2DFloat(MatrixFloat *matrixInput, FLOAT scalar, MatrixFloat *matrixResult, dispatch_queue_t queue) {
    int error = 0;
    if (matrixResult->height != matrixInput->height || matrixResult->width != matrixInput->width) {
        printf("matrix height must equal vector size and matrixInput must have same dimensions as matrixOutput\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in_scalar  = gcl_malloc(sizeof(cl_float), &scalar,
                                      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixInput->width, matrixInput->height, scalarMultiply2DFloat_kernel);
        
        scalarMultiply2DFloat_kernel(&range,(cl_float*)mem_in, (cl_float*)mem_in_scalar, (cl_float*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_float) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_in_scalar);
    gcl_free(mem_out);
    
    return error;
}

int scalarMultiply2DInt(MatrixInt *matrixInput, INT scalar, MatrixInt *matrixResult, dispatch_queue_t queue) {
    int error = 0;
    if (matrixResult->height != matrixInput->height || matrixResult->width != matrixInput->width) {
        printf("matrix height must equal vector size and matrixInput must have same dimensions as matrixOutput\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_int) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in_scalar  = gcl_malloc(sizeof(cl_int), &scalar,
                                      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixInput->width, matrixInput->height, scalarMultiply2DInt_kernel);
        scalarMultiply2DInt_kernel(&range,(cl_int*)mem_in, (cl_int*)mem_in_scalar, (cl_int*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_int) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_in_scalar);
    gcl_free(mem_out);
    
    return error;
}

#pragma mark Matrix Functions - Per-Element

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int add2DFloat(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->height != input2->height || input1->width != input2->width) {
        printf("add2DFloat: input matrix dimensions must match\n");
        return -1;
    }
    if (result->height != input1->height || result->width != input1->width) {
        printf("add2DFloat: result matrix dimensions must match input matrix dimensions\n");
        return -1;
    }
    
    void* mem_in1  = gcl_malloc(sizeof(cl_float) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_float) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * result->height * result->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, add2DFloat_kernel);
        add2DFloat_kernel(&range,(cl_float*)mem_in1, (cl_float*)mem_in2, (cl_float*)mem_out);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_float) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

int add2DInt(MatrixInt *input1, MatrixInt *input2, MatrixInt *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->height != input2->height || input1->width != input2->width) {
        printf("add2DInt: input matrix dimensions must match\n");
        return -1;
    }
    if (result->height != input1->height || result->width != input1->width) {
        printf("add2DInt: result matrix dimensions must match input matrix dimensions\n");
        return -1;
    }
    
    void* mem_in1  = gcl_malloc(sizeof(cl_int) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_int) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_int) * result->height * result->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, add2DInt_kernel);
        add2DInt_kernel(&range,(cl_int*)mem_in1, (cl_int*)mem_in2, (cl_int*)mem_out);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_int) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

int subtract2DFloat(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->height != input2->height || input1->width != input2->width) {
        printf("subtract2DFloat: input matrix dimensions must match\n");
        return -1;
    }
    if (result->height != input1->height || result->width != input1->width) {
        printf("subtract2DFloat: result matrix dimensions must match input matrix dimensions\n");
        return -1;
    }
    
    void* mem_in1  = gcl_malloc(sizeof(cl_float) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_float) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * result->height * result->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, subtract2DFloat_kernel);
        subtract2DFloat_kernel(&range,(cl_float*)mem_in1, (cl_float*)mem_in2, (cl_float*)mem_out);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_float) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

int subtract2DInt(MatrixInt *input1, MatrixInt *input2, MatrixInt *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->height != input2->height || input1->width != input2->width) {
        printf("subtract2DInt: input matrix dimensions must match\n");
        return -1;
    }
    if (result->height != input1->height || result->width != input1->width) {
        printf("subtract2DInt: result matrix dimensions must match input matrix dimensions\n");
        return -1;
    }
    
    void* mem_in1  = gcl_malloc(sizeof(cl_int) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_int) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_int) * result->height * result->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, subtract2DInt_kernel);
        subtract2DInt_kernel(&range,(cl_int*)mem_in1, (cl_int*)mem_in2, (cl_int*)mem_out);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_int) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

int elementMultiply2DFloat(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->height != input2->height || input1->width != input2->width) {
        printf("elementMultiply2DFloat: input matrix dimensions must match\n");
        return -1;
    }
    if (result->height != input1->height || result->width != input1->width) {
        printf("elementMultiply2DFloat: result matrix dimensions must match input matrix dimensions\n");
        return -1;
    }
    
    void* mem_in1  = gcl_malloc(sizeof(cl_float) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_float) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * result->height * result->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, elementMultiply2DFloat_kernel);
        elementMultiply2DFloat_kernel(&range,(cl_float*)mem_in1, (cl_float*)mem_in2, (cl_float*)mem_out);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_float) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

int elementMultiply2DInt(MatrixInt *input1, MatrixInt *input2, MatrixInt *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->height != input2->height || input1->width != input2->width) {
        printf("elementMultiply2DInt: input matrix dimensions must match\n");
        return -1;
    }
    if (result->height != input1->height || result->width != input1->width) {
        printf("elementMultiply2DInt: result matrix dimensions must match input matrix dimensions\n");
        return -1;
    }
    
    void* mem_in1  = gcl_malloc(sizeof(cl_int) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_int) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_int) * result->height * result->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, elementMultiply2DInt_kernel);
        elementMultiply2DInt_kernel(&range,(cl_int*)mem_in1, (cl_int*)mem_in2, (cl_int*)mem_out);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_int) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

#pragma mark Matrix Functions - Vectors

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int sumRows2DFloat(MatrixFloat* matrixInput, MatrixFloat *vectorResult, dispatch_queue_t queue) {
    if (vectorResult->width != 1) {
        printf("sumRows2DFloat: result vector must be column\n");
        return -1;
    } else if (matrixInput->height != vectorResult->height) {
        printf("result vector size must match input matrix height\n");
        return -1;
    }

    void* mem_in  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    int width = matrixInput->width;
    void *mem_width = gcl_malloc(sizeof(cl_int), &width, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *mem_out = gcl_malloc(sizeof(cl_float) * vectorResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(vectorResult->height, sumRows2DFloat_kernel);
        
        sumRows2DFloat_kernel(&range,(cl_float*)mem_in, (cl_int*)mem_width, (cl_float*)mem_out);
        gcl_memcpy(vectorResult->contents, mem_out, sizeof(cl_float) * vectorResult->height);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_width);
    gcl_free(mem_out);
    return 0;
}

int sumRows2DInt(MatrixInt* matrixInput, MatrixInt *vectorResult, dispatch_queue_t queue) {
    if (vectorResult->width != 1) {
        printf("sumRows2DFloat: result vector must be column\n");
        return -1;
    } else if (matrixInput->height != vectorResult->height) {
        printf("result vector size must match input matrix height\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_int) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    int width = matrixInput->width;
    void *mem_width = gcl_malloc(sizeof(cl_int), &width, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *mem_out = gcl_malloc(sizeof(cl_int) * vectorResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(vectorResult->height, sumRows2DInt_kernel);
        
        sumRows2DInt_kernel(&range,(cl_int*)mem_in, (cl_int*)mem_width, (cl_int*)mem_out);
        gcl_memcpy(vectorResult->contents, mem_out, sizeof(cl_int) * vectorResult->height);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_width);
    gcl_free(mem_out);
    return 0;
}

int sumColumns2DFloat(MatrixFloat* matrixInput, MatrixFloat *vectorResult, dispatch_queue_t queue) {
    if (vectorResult->height != 1) {
        printf("sumRows2DFloat: result vector must be row\n");
        return -1;
    } else if (matrixInput->width != vectorResult->width) {
        printf("result vector size must match input matrix width\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    int height = matrixInput->height;
    void *mem_height = gcl_malloc(sizeof(cl_int), &height, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *mem_out = gcl_malloc(sizeof(cl_float) * vectorResult->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(vectorResult->width, sumColumns2DFloat_kernel);
        
        sumColumns2DFloat_kernel(&range,(cl_float*)mem_in, (cl_int*)mem_height, (cl_float*)mem_out);
        gcl_memcpy(vectorResult->contents, mem_out, sizeof(cl_float) * vectorResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_height);
    gcl_free(mem_out);
    return 0;
}

int sumColumns2DInt(MatrixInt* matrixInput, MatrixInt *vectorResult, dispatch_queue_t queue) {
    if (vectorResult->height != 1) {
        printf("sumRows2DFloat: result vector must be row\n");
        return -1;
    } else if (matrixInput->width != vectorResult->width) {
        printf("result vector size must match input matrix width\n");
        return -1;
    }
    
    void* mem_in  = gcl_malloc(sizeof(cl_int) * matrixInput->width * matrixInput->height, matrixInput->contents,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    int height = matrixInput->height;
    void *mem_height = gcl_malloc(sizeof(cl_int), &height, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *mem_out = gcl_malloc(sizeof(cl_int) * vectorResult->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange1D(vectorResult->width, sumColumns2DInt_kernel);
        
        sumColumns2DInt_kernel(&range,(cl_int*)mem_in, (cl_int*)mem_height, (cl_int*)mem_out);
        gcl_memcpy(vectorResult->contents, mem_out, sizeof(cl_int) * vectorResult->width);
    });
    
    gcl_free(mem_in);
    gcl_free(mem_height);
    gcl_free(mem_out);
    return 0;
}

int vectorColumnAddToMatrixFloat(MatrixFloat *matrixInput, MatrixFloat *vector, MatrixFloat *matrixResult, dispatch_queue_t queue) {

    if (vector->width != 1) {
        printf("vectorColumnAddToMatrixFloat: vector must be column\n");
        return -1;
    } else if (matrixInput->height != vector->height || matrixResult->height != matrixInput->height || matrixResult->width != matrixInput->width) {
        printf("matrix height must equal vector size and matrixInput must have same dimensions as matrixOutput\n");
        return -1;
    }
    
    void* mem_inM  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_inV  = gcl_malloc(sizeof(cl_float) * vector->height, vector->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixResult->width, matrixResult->height, vectorColumnAddToMatrixFloat_kernel);
        
        vectorColumnAddToMatrixFloat_kernel(&range,(cl_float*)mem_inM, (cl_float*)mem_inV, (cl_float*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_float) * matrixResult->height * matrixResult->width);
    });
    
    gcl_free(mem_inM);
    gcl_free(mem_inV);
    gcl_free(mem_out);
    
    return 0;
}

int vectorRowAddToMatrixFloat(MatrixFloat *matrixInput, MatrixFloat *vector, MatrixFloat *matrixResult, dispatch_queue_t queue)  {

    if (vector->height != 1) {
        printf("vectorColumnAddToMatrixFloat: vector must be row\n");
        return -1;
    } else if (matrixInput->width != vector->width || matrixResult->height != matrixInput->height || matrixResult->width != matrixInput->width) {
        printf("matrix height must equal vector size and matrixInput must have same dimensions as matrixOutput\n");
        return -1;
    }
    
    void* mem_inM  = gcl_malloc(sizeof(cl_float) * matrixInput->width * matrixInput->height, matrixInput->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_inV  = gcl_malloc(sizeof(cl_float) * vector->width, vector->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * matrixResult->width * matrixResult->height, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(matrixResult->width, matrixResult->height, vectorRowAddToMatrixFloat_kernel);
        vectorRowAddToMatrixFloat_kernel(&range,(cl_float*)mem_inM, (cl_float*)mem_inV, (cl_float*)mem_out);
        gcl_memcpy(matrixResult->contents, mem_out, sizeof(cl_float) * matrixResult->height * matrixResult->width);
    });

    gcl_free(mem_inM);
    gcl_free(mem_inV);
    gcl_free(mem_out);
    
    return 0;
}

#pragma mark Matrix Functions - Two Matrices

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int multiply2DFloat(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *result, dispatch_queue_t queue) {
    return multiply2DFloat_GPU(input1, input2, result, queue);
}

int multiply2DInt(MatrixInt *input1, MatrixInt *input2, MatrixInt *result, dispatch_queue_t queue) {
    return multiply2DInt_GPU(input1, input2, result, queue);
}

int multiply2DFloat_GPU(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *result, dispatch_queue_t queue) {
    int error = 0;
    if (input1->width != input2->height) {
        printf("The width of the first matrix must match the height of the second matrix\n");
        printf("Inner dimensions don't match\n");
        return -1;
    }
    if (result->height != input1->height) {
        printf("The height of the result matrix must match the height of the first input\n");
        return -1;
    }
    if (result->width != input2->width) {
        printf("The width of the second input matrix must match the width of the result matrix\n");
        return -1;
    }
    int size = input1->width;
    int *sizePtr = &size;
    void* mem_in1  = gcl_malloc(sizeof(cl_float) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_float) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* sizePtrCL  = gcl_malloc(sizeof(cl_int), sizePtr,
                                  CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_float) * input1->height * input2->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, multiply2DFloat_kernel);
        multiply2DFloat_kernel(&range,(cl_float*)mem_in1, (cl_float*)mem_in2, (cl_float*)mem_out, (cl_int*)sizePtrCL);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_float) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

int multiply2DFloat_CPU(MatrixFloat *input1, MatrixFloat *input2, MatrixFloat *result) {
    if (input1->width != input2->height) {
        printf("The width of the first matrix must match the height of the second matrix\n");
        printf("Inner dimensions don't match\n");
        return -1;
    }
    if (result->height != input1->height) {
        printf("The height of the result matrix must match the height of the first input\n");
        return -1;
    }
    if (result->width != input2->width) {
        printf("The width of the second input matrix must match the width of the result matrix\n");
        return -1;
    }
    
    for (int row = 0; row < result->height; row++) {
        for (int column = 0; column < result->width; column++) {
            float dot = 0;
            for (int i = 0; i < input1->width; i++) {
                dot += input1->contents[input1->width * row + i] * input2->contents[input2->width * i + column];
            }
            result->contents[result->width * row + column] = dot;
        }
    }
    return 0;
}

int multiply2DInt_GPU(MatrixInt *input1, MatrixInt *input2, MatrixInt *result, dispatch_queue_t queue) {
    int error = 0;
    
    if (input1->width != input2->height) {
        printf("The width of the first matrix must match the height of the second matrix\n");
        printf("Inner dimensions don't match\n");
        return -1;
    }
    if (result->height != input1->height) {
        printf("The height of the result matrix must match the height of the first input\n");
        return -1;
    }
    if (result->width != input2->width) {
        printf("The width of the second input matrix must match the width of the result matrix\n");
        return -1;
    }
    int size = input1->width;
    int *sizePtr = &size;
    void* mem_in1  = gcl_malloc(sizeof(cl_int) * input1->width *input1->height, input1->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_in2  = gcl_malloc(sizeof(cl_int) * input2->width * input2->height, input2->contents,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* sizePtrCL  = gcl_malloc(sizeof(cl_int), sizePtr,
                                  CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void* mem_out =
    gcl_malloc(sizeof(cl_int) * input1->height * input2->width, NULL, CL_MEM_WRITE_ONLY);
    
    dispatch_sync(queue, ^{
        cl_ndrange range = createRange2D(result->width, result->height, multiply2DInt_kernel);
        multiply2DInt_kernel(&range,(cl_int*)mem_in1, (cl_int*)mem_in2, (cl_int*)mem_out, (cl_int*)sizePtrCL);
        gcl_memcpy(result->contents, mem_out, sizeof(cl_int) * input1->height * input2->width);
    });
    
    gcl_free(mem_in1);
    gcl_free(mem_in2);
    gcl_free(mem_out);
    
    return error;
}

#pragma mark Matrix Append Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int appendHorizontalFloat(MatrixFloat *leftMatrix, MatrixFloat *rightMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue) {
    return appendHorizontalFloat_CPU(leftMatrix, rightMatrix, resultMatrix, queue);
}

int appendHorizontalInt(MatrixInt *leftMatrix, MatrixInt *rightMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue) {
    return appendHorizontalInt_CPU(leftMatrix, rightMatrix, resultMatrix, queue);
}

int appendVerticalFloat(MatrixFloat *upperMatrix, MatrixFloat *lowerMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue) {
    return appendVerticalFloat_CPU(upperMatrix, lowerMatrix, resultMatrix, queue);
}

int appendVerticalInt(MatrixInt *upperMatrix, MatrixInt *lowerMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue) {
    return appendVerticalInt_CPU(upperMatrix, lowerMatrix, resultMatrix, queue);
}

int appendHorizontalFloat_CPU(MatrixFloat *leftMatrix, MatrixFloat *rightMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue) {
    if (leftMatrix->height != rightMatrix->height) {
        printf("appendHorizontalFloat: height of input matrices must match\n");
        return -1;
    } else if (leftMatrix->height != resultMatrix->height) {
        printf("appendHorizontalFloat: result matrix height must match input matrix heights\n");
        return -1;
    } else if (leftMatrix->width + rightMatrix->width != resultMatrix->width) {
        printf("appendHorizontalFloat: result matrix width must be sum of input matrix widths\n");
        return -1;
    }
    for (int row = 0; row < resultMatrix->height; row++) {
        for (int column = 0; column < resultMatrix->width * resultMatrix->height; column++) {
            if (column < leftMatrix->width) {
                resultMatrix->contents[resultMatrix->width * row + column] =
                leftMatrix->contents[leftMatrix->width * row + column];
                resultMatrix->contents[resultMatrix->width * row + column] =
                rightMatrix->contents[rightMatrix->width * row + column - leftMatrix->width];
            } else {
                resultMatrix->contents[resultMatrix->width * row + column] =
                rightMatrix->contents[rightMatrix->width * row + column - leftMatrix->width];
            }
        }
    }
    return 0;
}

int appendHorizontalInt_CPU(MatrixInt *leftMatrix, MatrixInt *rightMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue) {
    if (leftMatrix->height != rightMatrix->height) {
        printf("appendHorizontalInt: height of input matrices must match\n");
        return -1;
    } else if (leftMatrix->height != resultMatrix->height) {
        printf("appendHorizontalInt: result matrix height must match input matrix heights\n");
        return -1;
    } else if (leftMatrix->width + rightMatrix->width != resultMatrix->width) {
        printf("appendHorizontalInt: result matrix width must be sum of input matrix widths\n");
        return -1;
    }
    for (int row = 0; row < resultMatrix->height; row++) {
        for (int column = 0; column < resultMatrix->width * resultMatrix->height; column++) {
            if (column < leftMatrix->width) {
                resultMatrix->contents[resultMatrix->width * row + column] =
                leftMatrix->contents[leftMatrix->width * row + column];
                resultMatrix->contents[resultMatrix->width * row + column] =
                rightMatrix->contents[rightMatrix->width * row + column - leftMatrix->width];
            } else {
                resultMatrix->contents[resultMatrix->width * row + column] =
                rightMatrix->contents[rightMatrix->width * row + column - leftMatrix->width];
            }
        }
    }
    return 0;
}

int appendVerticalFloat_CPU(MatrixFloat *upperMatrix, MatrixFloat *lowerMatrix, MatrixFloat *resultMatrix, dispatch_queue_t queue) {
    if (upperMatrix->width != lowerMatrix->width) {
        printf("appendVerticalFloat: height of input matrices must match\n");
        return -1;
    } else if (upperMatrix->width != resultMatrix->width) {
        printf("appendVerticalFloat: result matrix height must match input matrix heights\n");
        return -1;
    } else if (upperMatrix->height + lowerMatrix->height != resultMatrix->height) {
        printf("appendVerticalFloat: result matrix width must be sum of input matrix widths\n");
        return -1;
    }
    for (int row = 0; row < resultMatrix->height; row++) {
        for (int column = 0; column < resultMatrix->width; column++) {
            if (row < upperMatrix->height) {
                resultMatrix->contents[resultMatrix->width * row + column] = upperMatrix->contents[upperMatrix->width * row + column];
            } else {
                resultMatrix->contents[resultMatrix->width * row + column] = lowerMatrix->contents[lowerMatrix->width * (row - upperMatrix->height) + column];
            }
        }
    }
    return 0;
}

int appendVerticalInt_CPU(MatrixInt *upperMatrix, MatrixInt *lowerMatrix, MatrixInt *resultMatrix, dispatch_queue_t queue) {
    if (upperMatrix->width != lowerMatrix->width) {
        printf("appendVerticalInt: height of input matrices must match\n");
        return -1;
    } else if (upperMatrix->width != resultMatrix->width) {
        printf("appendVerticalInt: result matrix height must match input matrix heights\n");
        return -1;
    } else if (upperMatrix->height + lowerMatrix->height != resultMatrix->height) {
        printf("appendVerticalInt: result matrix width must be sum of input matrix widths\n");
        return -1;
    }
    for (int row = 0; row < resultMatrix->height; row++) {
        for (int column = 0; column < resultMatrix->width; column++) {
            if (row < upperMatrix->height) {
                resultMatrix->contents[resultMatrix->width * row + column] = upperMatrix->contents[upperMatrix->width * row + column];
            } else {
                resultMatrix->contents[resultMatrix->width * row + column] = lowerMatrix->contents[lowerMatrix->width * (row - upperMatrix->height) + column];
            }
        }
    }
    return 0;
}

#pragma mark Determinant Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

int determinant2DFloat(MatrixFloat *matrix, FLOAT *result, dispatch_queue_t queue) {
    return determinantChio2DFloat(matrix, result, queue);
}

int determinant2DInt(MatrixInt *matrix, INT *result, dispatch_queue_t queue) {
    return determinantChio2DInt(matrix, result, queue);
}

/*
 *  Using Chio's condensation
 *  https://amca01.wordpress.com/2012/08/13/computing-determinants-elimination-and-condensation/
 */
int determinantChio2DFloat(MatrixFloat *matrix, FLOAT *result, dispatch_queue_t queue) {
    if (matrix->height != matrix->width) {
        printf("Determinant requires square matrix\n");
        return -1;
    }
    
    __block void *mem_matrix_out  = gcl_malloc(sizeof(cl_float) * matrix->width * matrix->height, matrix->contents,
                                               CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR);
    
    __block void *mem_matrix_in  = gcl_malloc(sizeof(cl_float) * matrix->width * matrix->height, NULL, CL_MEM_READ_WRITE);
    
    int width = matrix->width;
    void *mem_index_width = gcl_malloc(sizeof(cl_int), &width, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    printMatrixFloat(matrix);
    
    __block char leftColumnZero = false;
    
    __block double divisor = 1;
    MatrixFloat *temp2Matrix = createMatrixFloat(matrix->width, matrix->height);
    dispatch_sync(queue, ^{
        
        for (int matrix_size = matrix->width - 1; matrix_size > 1; matrix_size--) {
            float power = (float)(matrix_size - 1);
            
            cl_ndrange range = createRangeSquare2D(matrix_size, determinantChio2DFloat_kernel);
            
            float matrixZeroZero;
            gcl_memcpy(&matrixZeroZero, mem_matrix_out, sizeof(cl_float));
            
            float aZeroZeroPowed = 1;
            if (matrixZeroZero > -0.0001 && matrixZeroZero < 0.0001) {
                leftColumnZero = true;
                MatrixFloat *matrixOut = createMatrixFloat(matrix->width, matrix->height);
                gcl_memcpy(matrixOut->contents, mem_matrix_out, sizeof(cl_float) * matrixOut->width * matrixOut->height);
                for (int rowIndex = 1; rowIndex < matrixOut->height; rowIndex++) {
                    if (matrixOut->contents[matrixOut->width * rowIndex] < -0.001 || matrixOut->contents[matrixOut->width * rowIndex] > 0.001) {
                        matrixZeroZero = matrixOut->contents[matrixOut->width * rowIndex];
                        void *mem_row_to_add = gcl_malloc(sizeof(cl_float) * matrix->width, matrixOut->contents + matrixOut->width * rowIndex, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
                        cl_ndrange rangeArray = createRangeSquare2D(matrix->width, vectorRowAddToMatrixFloat_kernel);
                        vectorRowAddToMatrixFloat_kernel(&rangeArray, mem_row_to_add, mem_matrix_out, mem_matrix_in);
                        gcl_free(mem_row_to_add);
                        leftColumnZero = false;
                        aZeroZeroPowed = powf(matrixZeroZero, power);
                        break;
                    }
                }
                if (leftColumnZero == true) {
                    break;
                }
            } else {
                aZeroZeroPowed = pow((double)matrixZeroZero, (double)power);
                
                void *temp = mem_matrix_in;
                mem_matrix_in = mem_matrix_out;
                mem_matrix_out = temp;
            }
            if (leftColumnZero == false) {
                range = createRangeSquare2D(matrix_size, determinantChio2DFloat_kernel);
                determinantChio2DFloat_kernel(&range,(cl_float *)mem_matrix_in, (cl_float *)mem_matrix_out, (cl_int *)mem_index_width);
                divisor *= aZeroZeroPowed;
            }
        }
        gcl_memcpy(temp2Matrix->contents, mem_matrix_out, sizeof(cl_float) * matrix->width * matrix->height);
    });
    
    if (leftColumnZero == false) {
        float det = ((temp2Matrix->contents[temp2Matrix->width * 0 + 0] * temp2Matrix->contents[temp2Matrix->width * 1 + 1] -
                      temp2Matrix->contents[temp2Matrix->width * 0 + 1] * temp2Matrix->contents[temp2Matrix->width * 1 + 0])) / divisor;
        *result = det;
    } else {
        *result = 0;
    }
    
    gcl_free(mem_matrix_in);
    gcl_free(mem_matrix_out);
    deleteMatrixFloat(temp2Matrix);
    return 0;
}

/*
 *  Using Chio's condensation
 *  https://amca01.wordpress.com/2012/08/13/computing-determinants-elimination-and-condensation/
 */
int determinantChio2DInt(MatrixInt *matrix, INT *result, dispatch_queue_t queue) {
    if (matrix->height != matrix->width) {
        printf("Determinant requires square matrix\n");
        return -1;
    }
    
    __block void *mem_matrix_out  = gcl_malloc(sizeof(cl_float) * matrix->width * matrix->height, matrix->contents,
                                               CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR);
    
    __block void *mem_matrix_in  = gcl_malloc(sizeof(cl_float) * matrix->width * matrix->height, NULL, CL_MEM_READ_WRITE);
    
    int width = matrix->width;
    void *mem_index_width = gcl_malloc(sizeof(cl_int), &width, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    printMatrixInt(matrix);
    
    __block char leftColumnZero = false;
    __block double divisor = 1;
    MatrixInt *temp2Matrix = createMatrixInt(matrix->width, matrix->height);
    dispatch_sync(queue, ^{
        
        for (int matrix_size = matrix->width - 1; matrix_size > 1; matrix_size--) {
            double power = (double)(matrix_size - 1);
            cl_ndrange range = createRangeSquare2D(matrix_size, determinantChio2DFloat_kernel);
            
            int matrixZeroZero;
            gcl_memcpy(&matrixZeroZero, mem_matrix_out, sizeof(cl_int));
            
            float aZeroZeroPowed = 1;
            if (matrixZeroZero == 0) {
                leftColumnZero = true;
                MatrixFloat *matrixOut = createMatrixFloat(matrix->width, matrix->height);
                gcl_memcpy(matrixOut->contents, mem_matrix_out, sizeof(cl_int) * matrixOut->width * matrixOut->height);
                for (int rowIndex = 1; rowIndex < matrixOut->height; rowIndex++) {
                    if (matrixOut->contents[matrixOut->width * rowIndex] != 0) {
                        matrixZeroZero = matrixOut->contents[matrixOut->width * rowIndex];
                        void *mem_row_to_add = gcl_malloc(sizeof(cl_int) * matrix->width, matrixOut->contents + matrixOut->width * rowIndex, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
                        cl_ndrange rangeArray = createRangeSquare2D(matrix->width, vectorRowAddToMatrixInt_kernel);
                        vectorRowAddToMatrixInt_kernel(&rangeArray, mem_row_to_add, mem_matrix_out, mem_matrix_in);
                        gcl_free(mem_row_to_add);
                        leftColumnZero = false;
                        aZeroZeroPowed = powf(matrixZeroZero, power);
                        break;
                    }
                }
                if (leftColumnZero == true) {
                    break;
                }
            } else {
                aZeroZeroPowed = pow((double)matrixZeroZero, (double)power);
                
                void *temp = mem_matrix_in;
                mem_matrix_in = mem_matrix_out;
                mem_matrix_out = temp;
            }
            if (leftColumnZero == false) {
                range = createRangeSquare2D(matrix_size, determinantChio2DInt_kernel);
                determinantChio2DInt_kernel(&range,(cl_int *)mem_matrix_in, (cl_int *)mem_matrix_out, (cl_int *)mem_index_width);
                divisor *= aZeroZeroPowed;
            }
        }
        gcl_memcpy(temp2Matrix->contents, mem_matrix_out, sizeof(cl_float) * matrix->width * matrix->height);
    });
    
    if (leftColumnZero == false) {
        double det = (((double)temp2Matrix->contents[temp2Matrix->width * 0 + 0] * (double)temp2Matrix->contents[temp2Matrix->width * 1 + 1] -
                       (double)temp2Matrix->contents[temp2Matrix->width * 0 + 1] * (double)temp2Matrix->contents[temp2Matrix->width * 1 + 0])) / divisor;
        *result = (int)det;
    } else {
        *result = 0;
    }
    
    gcl_free(mem_matrix_in);
    gcl_free(mem_matrix_out);
    deleteMatrixInt(temp2Matrix);
    return 0;
}

#pragma mark Helper Functions

/********************************************************************************
 *
 *
 *
 ********************************************************************************/

cl_ndrange createRange1D(int size, void *kernelFunction) {
    size_t wgs;
    gcl_get_kernel_block_workgroup_info(kernelFunction,
                                        CL_KERNEL_WORK_GROUP_SIZE,
                                        sizeof(wgs), &wgs, NULL);
    cl_ndrange range = {
        1,                      // The number of dimensions to use.
        
        {0, 0, 0},              //offsets
        
        {size, 0, 0},     //global range
        
        {0/*wgs*/, 0/*0*/, 0}             //work group size
        //if work group size is 0 OpenCL will choose the best size
    };
    return range;
}

cl_ndrange createRangeSquare2D(int size, void *kernelFunction) {
    return createRange2D(size, size, kernelFunction);
}

cl_ndrange createRange2D(int width, int height, void *kernelFunction) {
    size_t wgs;
    gcl_get_kernel_block_workgroup_info(kernelFunction,
                                        CL_KERNEL_WORK_GROUP_SIZE,
                                        sizeof(wgs), &wgs, NULL);
    cl_ndrange range = {
        2,                      // The number of dimensions to use.
        
        {0, 0, 0},              //offsets
        
        {width, height, 0},     //global range
        
        {0/*wgs*/, 0/*0*/, 0}             //work group size
        //if work group size is 0 OpenCL will choose the best size
    };
    return range;
}
