//
//  matrixtypes.h
//  ANN
//
//  Created by Nicholas Wilkerson and Dave Heyborne on 12/30/14.
//  Copyright (c) 2014 Nick Wilkerson and Dave Heyborne. All rights reserved.
//

#ifndef ANN_matrixtypes_h
#define ANN_matrixtypes_h

#define INT int
#define FLOAT float

typedef struct MatrixFloat {
    FLOAT *contents;
    INT width;
    INT height;
} MatrixFloat;

typedef struct MatrixInt {
    INT *contents;
    INT width;
    INT height;
} MatrixInt;

typedef enum VectorOrientation {
    ROW,
    COLUMN
} VectorOrientation;

#endif
